/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matheus igor ricardo
 */
public class Manipula {
    
    public Manipula(){
        
    }
    
    /**
     * Função para salvar médico no arquivo
     * @param med
     * @param arq
     */
    public static void salvaMedico(Medico med, String arq) {
       
        FileOutputStream file;
        
        try {
            file = new FileOutputStream(arq, true);//abre o arquivo no modo append. Se não existir, ele é criado
            PrintStream out = new PrintStream(file);//objeto que faz a escrita
            
            out.println(med.getNome());//escreve o nome do médico no arquivo
            out.println(med.getCRM());//escreve o crm do médico no arquivo
            out.println(med.getEspecialidade());//escreve a especialidade do médico no arquivo
            out.println(med.getDataNasc());//escreve a data de nascimento do médico no arquivo
            out.println(med.getTel());//escreve o telefone do médico no arquivo
            for(String str: med.getDiasDeTrabalho()){//percorre a lista de dias de trabalho do médico
                out.println(str);//escreve o dia no arquivo (1 = segunda, 2 = terça...)
            }
            out.println();//escreve um "\n" simbolizando um terminador
            
            file.close();//fecha o arquivo
            System.out.println("Medico salvo com sucesso");//mensagem de sucesso no console
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Manipula.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Manipula.class.getName()).log(Level.SEVERE, null, ex);
        } 
      
    }
    
    //método que encontra um médico no arquivo. 
    //obs: acho que não precisará dessa função
    public static void encontraMedico(String nome, String arq){
        Scanner scanner;
        String linha;
        
        try {
            scanner = new Scanner(new FileReader(arq)).useDelimiter("\n");
            
            while(scanner.hasNext()){
                linha = scanner.next();
                if(linha.equals(nome)){
                    System.out.println(linha);
                    return;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Manipula.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loadMedicos(String arq){
        Scanner scanner;
        
        try {
            scanner = new Scanner(new FileReader(arq)).useDelimiter("\n");
            
            
            while(scanner.hasNext()){
                String nome = scanner.next();//lê o nome do médico
                ClinicaSaraCura.nomesMedicos.add(nome); //adiciona na lista de nomes de médicos
                scanner.next(); //lê o crm
                String especialidade = scanner.next();//lê a especialidade
                ClinicaSaraCura.todasEspecialidades.add(especialidade);//adiciona na lista de especialidades
                String dataNasc = scanner.next();//lê a data de nascimento
                ClinicaSaraCura.mapMedicoDataNasc.put(nome,dataNasc);
                String tel = scanner.next();//lê o telefone
                ClinicaSaraCura.mapMedicoTel.put(nome,tel);
                String dia = scanner.next();//lê o dia de trabalho 1
                ClinicaSaraCura.mapMedicosDiaDeTrabalho.put(nome,dia);
                dia = scanner.next();//lê o dia de trabalho 2
                ClinicaSaraCura.mapMedicosDiaDeTrabalho.put(nome,dia);
                dia = scanner.next();//lê o dia de trabalho 3
                ClinicaSaraCura.mapMedicosDiaDeTrabalho.put(nome,dia);
                String horaInicial = scanner.next(); //lê a hora inicial de trabalho
                ClinicaSaraCura.mapMedicoHoraInicio.put(nome, horaInicial);
                scanner.next();//lê o separador \n
                //System.out.println(nomesMedicos.get(i));
                
                ClinicaSaraCura.mapEspecialiadeMedicos.put(especialidade,nome);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Manipula.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        //ordena os nomes dos médicos
        Collections.sort(ClinicaSaraCura.nomesMedicos);
        
        //cria um set para armazenar os nomes de especialidades dos médicos. Set pois não aceita entrada duplicada
        Set<String> keys = ClinicaSaraCura.mapEspecialiadeMedicos.keySet();
        for (String key : keys) {//para cada especialidade, recebe a lista com os médicos dessa especialiade
            Collections.sort((List<String>) ClinicaSaraCura.mapEspecialiadeMedicos.get(key));//ordena os médicos por nome
        }
       
    }
    
    
}
