/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

/**
 *
 * @author matheus igor ricardo
 */
public class Medico {
        
    private String nome;
    private String crm;
    private String tel;
    private String data_nasc;
    private String especialidade;
    private String diaDeTrabalho1,diaDeTrabalho2,diaDeTrabalho3;
    
    public Medico(){//construtor padrão
        
    }
    
    
    protected void setNome(String nome){
        this.nome = nome;
    }
    
    protected void setCRM(String crm){
        this.crm = crm;
    } 
    
    protected void setTel(String tel){
        this.tel = tel;
    }
    
    protected void setDataNasc(String data_nasc){
        this.data_nasc = data_nasc;
    }
    
    protected void setEspecialidade(String especialidade){
        this.especialidade = especialidade;
    }
    
    protected void setDiasDeTrabalho(String[] diasDeTrabalho){
        this.diaDeTrabalho1 = diasDeTrabalho[0];
        this.diaDeTrabalho2 = diasDeTrabalho[1];
        this.diaDeTrabalho3 = diasDeTrabalho[2];
    }
    
    
    public String getNome(){
        return nome;
    }
    
    public String getCRM(){
        return crm;
    }
    
    public String getTel(){
        return tel;
    }
    
    public String getDataNasc(){
        return data_nasc;
    }
    
    public String getEspecialidade(){
        return especialidade;
    }
    
    public String[] getDiasDeTrabalho(){
        String[] diasDeTrabalho = {this.diaDeTrabalho1,this.diaDeTrabalho2,this.diaDeTrabalho3};
        return diasDeTrabalho;
    }
    
    @Override
    public String toString(){
        return "Nome: " + nome + "\nCRM: " + crm + "\nTelefone: " +
                tel + "\nData de nascimento: " + data_nasc + "\nDias de trabalho: " + diaDeTrabalho1 + ", " + 
                diaDeTrabalho2 + ", " + diaDeTrabalho3;
    }
}
