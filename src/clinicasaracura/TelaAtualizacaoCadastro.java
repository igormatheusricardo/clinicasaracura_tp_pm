/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

import java.util.Collection;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author matheus
 */
public class TelaAtualizacaoCadastro extends javax.swing.JFrame {

    /**
     * Creates new form TelaAtualizacaoCadastro
     */
    public TelaAtualizacaoCadastro() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelSelecioneMedico = new javax.swing.JLabel();
        labelAgendarConsulta = new javax.swing.JLabel();
        separadorAgendarConsulta = new javax.swing.JSeparator();
        comboBoxSelecioneMedico = new javax.swing.JComboBox<>();
        buttonCancelarAtualizacaoCadastro = new javax.swing.JButton();
        buttonConfirmarAtualizacaoCadastro = new javax.swing.JButton();
        labelNome = new javax.swing.JLabel();
        textFieldNome = new javax.swing.JTextField();
        labelEspecialidade = new javax.swing.JLabel();
        textFieldEspecialidade = new javax.swing.JTextField();
        labelTelefone = new javax.swing.JLabel();
        textFieldTelefone = new javax.swing.JTextField();
        labelDataNasc = new javax.swing.JLabel();
        textFieldDataNasc = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Atualização de Cadastro");
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(500, 365));
        setMinimumSize(new java.awt.Dimension(500, 365));

        labelSelecioneMedico.setText("Selecione o médico:");

        labelAgendarConsulta.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        labelAgendarConsulta.setText("Atualização de Cadastro");

        comboBoxSelecioneMedico.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Selecione o Médico--" }));
        comboBoxSelecioneMedico.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                comboBoxSelecioneMedicoFocusGained(evt);
            }
        });
        comboBoxSelecioneMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxSelecioneMedicoActionPerformed(evt);
            }
        });

        buttonCancelarAtualizacaoCadastro.setForeground(new java.awt.Color(249, 18, 18));
        buttonCancelarAtualizacaoCadastro.setText("Cancelar");
        buttonCancelarAtualizacaoCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarAtualizacaoCadastroActionPerformed(evt);
            }
        });

        buttonConfirmarAtualizacaoCadastro.setText("Confirmar");
        buttonConfirmarAtualizacaoCadastro.setEnabled(false);
        buttonConfirmarAtualizacaoCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonConfirmarAtualizacaoCadastroActionPerformed(evt);
            }
        });

        labelNome.setText("Nome:");

        labelEspecialidade.setText("Especialidade:");

        labelTelefone.setText("Telefone:");

        labelDataNasc.setText("Data de Nasc:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(labelAgendarConsulta)
                .addGap(138, 138, 138))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonConfirmarAtualizacaoCadastro)
                .addGap(85, 85, 85)
                .addComponent(buttonCancelarAtualizacaoCadastro)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(separadorAgendarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 455, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(labelSelecioneMedico)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboBoxSelecioneMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(labelNome)
                                    .addGap(6, 6, 6)
                                    .addComponent(textFieldNome))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(labelEspecialidade)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(textFieldEspecialidade, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(labelDataNasc)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(textFieldDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(labelTelefone)
                                .addGap(18, 18, 18)
                                .addComponent(textFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelAgendarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separadorAgendarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelSelecioneMedico)
                    .addComponent(comboBoxSelecioneMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelNome)
                    .addComponent(textFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelEspecialidade)
                    .addComponent(textFieldEspecialidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTelefone)
                    .addComponent(textFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDataNasc)
                    .addComponent(textFieldDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonConfirmarAtualizacaoCadastro, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(buttonCancelarAtualizacaoCadastro))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxSelecioneMedicoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboBoxSelecioneMedicoFocusGained
        populaMedicos();
    }//GEN-LAST:event_comboBoxSelecioneMedicoFocusGained

    private void buttonCancelarAtualizacaoCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelarAtualizacaoCadastroActionPerformed
        int opcao = JOptionPane.showConfirmDialog(null,"Deseja mesmo cancelar?", "Atenção", JOptionPane.YES_NO_OPTION);
        if(opcao == 0){
            TelaInicial.telaAgendamento = null;//para possibilitar reabrir a janela
            dispose();//fecha a janela
        }
    }//GEN-LAST:event_buttonCancelarAtualizacaoCadastroActionPerformed

    private void buttonConfirmarAtualizacaoCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonConfirmarAtualizacaoCadastroActionPerformed
        int resposta = JOptionPane.showConfirmDialog(null,"Confirmar atualização?", "Atualização", JOptionPane.YES_NO_OPTION);
        if(resposta == 0){//Respondeu sim
            JOptionPane.showMessageDialog(null, "Dados atualizados com sucesso!");
            this.dispose();
        }
    }//GEN-LAST:event_buttonConfirmarAtualizacaoCadastroActionPerformed

    private void comboBoxSelecioneMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxSelecioneMedicoActionPerformed
        if(comboBoxSelecioneMedico.getSelectedIndex()!=0){
            //set nome 
            String nome = comboBoxSelecioneMedico.getSelectedItem().toString();
            textFieldNome.setText(comboBoxSelecioneMedico.getSelectedItem().toString());
            
            //set especialidade
            Set<String> keys = ClinicaSaraCura.mapEspecialiadeMedicos.keySet();
            for (String key : keys) { //para cada especialidade, recebe a lista com os médicos dessa especialiade
                Collection<String> get = ClinicaSaraCura.mapEspecialiadeMedicos.get(key); 
                if(get.contains(nome)){
                    textFieldEspecialidade.setText(key);
                }
            }
            
            //set telefone
            textFieldTelefone.setText(ClinicaSaraCura.mapMedicoTel.get(nome));
            
            //set dataNasc
            textFieldDataNasc.setText(ClinicaSaraCura.mapMedicoDataNasc.get(nome));
            
            //habilita confirmar
            buttonConfirmarAtualizacaoCadastro.setEnabled(true);
        }else{
            textFieldNome.setText("");
            textFieldEspecialidade.setText("");
            textFieldTelefone.setText("");
            textFieldDataNasc.setText("");
            buttonConfirmarAtualizacaoCadastro.setEnabled(false);
        }
    }//GEN-LAST:event_comboBoxSelecioneMedicoActionPerformed

    /**
     */
    public static void main() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaAtualizacaoCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaAtualizacaoCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaAtualizacaoCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaAtualizacaoCadastro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new TelaAtualizacaoCadastro().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCancelarAtualizacaoCadastro;
    private javax.swing.JButton buttonConfirmarAtualizacaoCadastro;
    private javax.swing.JComboBox<String> comboBoxSelecioneMedico;
    private javax.swing.JLabel labelAgendarConsulta;
    private javax.swing.JLabel labelDataNasc;
    private javax.swing.JLabel labelEspecialidade;
    private javax.swing.JLabel labelNome;
    private javax.swing.JLabel labelSelecioneMedico;
    private javax.swing.JLabel labelTelefone;
    private javax.swing.JSeparator separadorAgendarConsulta;
    private javax.swing.JTextField textFieldDataNasc;
    private javax.swing.JTextField textFieldEspecialidade;
    private javax.swing.JTextField textFieldNome;
    private javax.swing.JTextField textFieldTelefone;
    // End of variables declaration//GEN-END:variables

    //função que popula o combobox com os médicos da especialidade clicada
    private void populaMedicos() {
        //recebe a lista com os médicos da especialidade dada no parâmetro da função
        Collection<String> medicos = ClinicaSaraCura.nomesMedicos;        
       
        medicos.forEach((medico) -> {
            //para cada médico existente na especialidade selecionada
            comboBoxSelecioneMedico.addItem(medico);//adiciona o médico na lista do jComboBox2 (selecionar médico)
        });
        
    }

}