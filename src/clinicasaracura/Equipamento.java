/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

/**
 *
 * @author matheus igor ricardo
 */
public class Equipamento {
    
    public enum TIPOEQUIP {//enum para os tipos de equipamento existentes
        TOMOGRAFIA, RESSONANCIA, ULTRASSOM, OUTRO;
    }
    
    private TIPOEQUIP tipoEquip;
    
    public Equipamento(){//construtor padrão
        
    }
    
    public TIPOEQUIP getTipoExame(){
        return tipoEquip;
    }
    
    private void setTipoExame(TIPOEQUIP tipoEquip){
        this.tipoEquip = tipoEquip;
    }
    
}
