/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author matheus igor ricardo
 */
public class Agenda {
    
    public enum tipoAgenda{//2 tipos de agenda: medico e equipamento. Utilizado para chamar o construtor adequado
        MEDICO, EQUIPAMENTO;
    }
    
    protected Map<String, List<String>> compromissos;//mapear a lista de compromissos aos médicos 
    
    //construtor
    public Agenda(tipoAgenda tipo){//recebe tipoAgenda para definir se será agenda de médico ou equipamento
        compromissos = new HashMap<>();
        String nomeArquivo = null;
        
        switch (tipo) {
            case MEDICO://se for agenda de médico, utilizar o arquivo AgendaMedicos.txt
                nomeArquivo = "AgendaMedicos.txt";
                break;
            case EQUIPAMENTO://se for agenda do equipamento, utilizar o arquivo AgendaEquipamentos.txt
                nomeArquivo = "AgendaEquipamentos.txt";
                break;
            default:
                System.out.println("Erro inesperado! tipo de agenda invalido");
                System.exit(1);
                break;
        }
        this.loadAgenda(nomeArquivo);//chama a rotina que faz a leitura do arquivo
    }
    
    //lê o arquivo da agenda e atribui os valores às variáveis
    private void loadAgenda(String nomeArquivo){
            
        FileOutputStream file = null;
        try {
            //tenta abrir o arquivo apenas para que caso não exista, seja criado e não ocorra erro por ausência de arquivo
            file = new FileOutputStream(nomeArquivo,true);//true = append
            file.close();//fecha o arquivo
            
            
            FileReader fr = null;
            Scanner scanner = null;
            fr = new FileReader(nomeArquivo);//abre o arquivo para leitura
            scanner = new Scanner(fr).useDelimiter("\n");//define o objeto scanner que fará a leitura do arquivo utilizando como delimitador o "\n". Ou seja, lerá linha por linha
            String linha = null;
            while(scanner.hasNext()){//enquanto houver próxima linha para ler
   
                linha = scanner.next();//lê a linha e atribui na variável linha
                
                String[] split;//Array de strings que fará o trim da string lida
                split = linha.split(";");//define o delimitador para o trim e realiza o trim atribuindo ao array split
                
                List<String> horarios;//cria um array de string que conterá todos os compromissos do médico
                horarios = new ArrayList<>();
                for(int i = 1; i < split.length; i++){//percorre todos os compromissos. inicia de 1 pois o 0 contém o nome do médico
                    horarios.add(split[i]);//adiciona o compromisso no array horarios
                }
                compromissos.put(split[0], horarios);//mapeia os compromissos ao médico (split[0] contém o nome do médico)
                
            }
            fr.close();//fecha o arquivo
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    public List<String> getCompromissosMedico(String nome){//retorna os compromissos do médico enviado como parâmetro
        return compromissos.get(nome);
    }
    
    
    
}
