/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinicasaracura;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author matheus igor ricardo
 */
public class ClinicaSaraCura {

    public static Agenda agendaMedicos;
    public static Agenda agendaEquipamentos;
    public static Set<String> todasEspecialidades;
    public static ArrayList<String> nomesMedicos;
    public static Multimap<String, String> mapEspecialiadeMedicos;
    public static Multimap<String, String> mapMedicosDiaDeTrabalho;
    public static Map<String,String> mapMedicoHoraInicio;
    public static Map<String,String> mapMedicoDataNasc;
    public static Map<String,String> mapMedicoTel;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*TelaInicial telaInicial = new TelaInicial();
        TelaInicial.main(args);
        
        Medico med = new Medico();
        med.setNome("Teste Testador Testando");
        med.setCRM("192347198");
        med.setDataNasc("13/13/1989");
        med.setTel("(31)9 9992-4713");
        med.setEspecialidade("Sistamas de Informação");
        Medico.diasDaSemana[] diasDeTrabalho = {Medico.diasDaSemana.SABADO, Medico.diasDaSemana.QUARTA, Medico.diasDaSemana.QUINTA};
        med.setDiasDeTrabalho(diasDeTrabalho);
        System.out.println(med+"\n");
*/
        
        //Manipula.salvaMedico(med, "Medicos.txt");
        //Manipula.encontraMedico("João da Silva", "Medicos.txt");
        
        
        agendaMedicos = new Agenda(Agenda.tipoAgenda.MEDICO);
        agendaEquipamentos = new Agenda(Agenda.tipoAgenda.EQUIPAMENTO);
        todasEspecialidades = new HashSet<>();
        nomesMedicos = new ArrayList<>();
        mapEspecialiadeMedicos = ArrayListMultimap.create();
        mapMedicosDiaDeTrabalho = ArrayListMultimap.create();
        mapMedicoHoraInicio = new HashMap<>();
        mapMedicoTel = new HashMap<>();
        mapMedicoDataNasc = new HashMap<>();
        
        
        Manipula.loadMedicos("Medicos.txt");//Faz o carregamento dos arquivos na memória
        
        //Cria a tela inicial
        TelaInicial telaInicial = new TelaInicial();
        TelaInicial.main(args);
    }
    
}
